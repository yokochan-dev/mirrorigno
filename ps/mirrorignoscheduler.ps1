param(
    [Parameter(Mandatory)][string]$Operation
    [Parameter(Mandatory)][string]$LocalPath
)

$taskname=mirrorigno+$LocalPath
try{
    Get-ScheduledTask -TaskName $taskname
    $exists=$True
}
catch{
    $exists=$False
}

switch[-exact]($operation){
    "create"{
        if($exists){
            Write-Host "Scheduled task already exists!"
            exit
        }
        Write-Host "Creating task with taskname $taskname"
        $taskaction=New-ScheduledTaskAction -Execute "powershell.exe" -Arguments "Update "$LocalPath""
        $timespan=New-TimeSpan -Hours 1
        $tasktrigger=New-SchedultedTaskTrigger -RepetitionInterval $timespan -AtLogon
        Register-ScheduledTask -TaskName $taskname -TaskAction $taskaction -TaskTrigger $tasktrigger
        Start-ScheduledTask -TaskName $taskname
        Write-Host "Task $taskname registered and running."
        break
    }
    "status"{
        if($exists){
            Get-SchedultedTask -TaskName $taskname
            exit
        }
        else{
            echo "Task $taskname does not exist."
            exit
        }
    }
    "delete"{
        if($exists){
            Unregister-ScheduledTask -TaskName $taskname
            echo "Task $taskname unregistered."
            exit
        }
        else{
            echo "Task $taskname does not exist."
            exit
        }
    }
}
