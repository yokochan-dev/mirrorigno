# param(
#     [Parameter(Mandatory)][string]$Operation,
#     [Parameter(Mandatory)][string[]]$Args
# )

function Find-Git(){
    foreach($path in ($Env:ProgramFiles).split(";")){
        $flag=Get-ChildItem -Path $path -Recurse -ErrorAction SilentlyContinue -Filter "git.exe"
        if([string]::isNullOrEmpty($flag)){
            Write-Host "Git doesn't appear to be in $Env:ProgramFiles."
            return $False
        }
    }
    return $True
}

function Initialize-Mirror(){

    param(
        [Parameter(Mandatory)][string]$LocalPath,
        [Parameter(Mandatory)][string]$OriginUrl,
        [Parameter(Mandatory)][string]$MirrorUrl
    )

    if((Test-Path -Path $LocalPath) -eq $False){
        Write-Host "$LocalPath is not a valid path. Exiting program."
        Exit
    }
    Write-Host "Cloning the original repository..."
    git clone --mirror "$OriginUrl" "$LocalPath"
    if($LastExitCode){
        Write-Host "Cloning failed. Closing program."
        exit
    }
    Write-Host "Repository succesfully cloned."
    cd $LocalPath
    git remote add --mirror=fetch mirror "$MirrorUrl"
    Update-Mirror -LocalPath $LocalPath
    return
}

function Update-Mirror(){

    param(
        [Parameter(Mandatory)][string]$LocalPath
    )

    if((Test-Path -Path $LocalPath) -eq $False){
        Write-Host "$LocalPath is not a valid path. Exiting program."
        exit
    }
    Write-Host "Fetching resources from original repository..."
    cd $LocalPath
    git status
    git fetch origin
    git push mirror --all --force
    return
}

Write-Host "Welcome to mirrorigno!"
if(-Not (Find-Git)){
    exit
}
Write-Host "Checking arguments..."
$operation=$args[0]
if($operation -eq "Initialize"){
    Initialize-Mirror -LocalPath $args[1] -OriginUrl $args[2] -MirrorUrl $args[3]
}
elseif($operation -eq "Update"){
    Update-Mirror -LocalPath $args[1]
}
else{
    Write-Host "Insert valid arguments."
    exit
}
