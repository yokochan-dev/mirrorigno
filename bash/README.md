# mirrorigno

A tool for automatically mirroring git repositories.

## Requirements

- Bash
- Git binary in the system path.

## Usage

The bash folder contains one script. `mirrorigno.bash` is the script containing the logic of the program.<br>
To set up the program:<br>
`.\mirrorigno.bash initialize <path\to\local\repo> <url\to\clone> <url\to\mirror>`<br>
To update the repo:<br>
`.\mirrorigno.bash update <path\to\local\repo>`<br>
Use double quotes for the path if necessary.

### (Optional) Set up service

You can optionally setup a service that starts 10 minutes after boot and runs hourly.<br>
The script won't work if you haven't initialized the program first. See usage.<br>
It is necessary to have mirrorigno.bash in the $PATH.<br>
Remember to add the local directory to the .service file.

## License

See LICENSE.txt file in the root directory.
