#!/bin/bash 

findGit(){
    local exists
    if [ -x "/usr/bin/git" ]; then
        exists="true"
	fi	
	echo exists
}

initializeMirror(){
	
	local originUrl="$2"
	local localPath="$1"
	local remoteUrl="$3"
	if [ -d "$localPath" ]; then
		echo "Cloning repository..."
		git clone --mirror "$originUrl" "$localPath"
		if [ "$?" -ne 0 ]; then
			echo "Cloning failed. Closing program."
			exit
		fi
		echo "Repository succesfully cloned."
		cd "$localPath"
		echo "Mirroring the repository."
		git remote add --mirror=fetch mirror "$remoteUrl"
		updateMirror "$localPath"
		return 0
	else
		echo "Local path $localPath doesn't exist"
		exit
	fi

}

updateMirror(){

	local localPath="$1"
	if [ -d "$localPath" ]; then
		echo "Fetching resources from original repository..."
		cd "$localPath"
		git status
		git fetch origin
		git push mirror --all --force
		return 0
	else
		echo "Directory $localPath is not valid."
		exit

}

echo "Welcome to mirrorigno!"
gitExists=$(findGit)
if [ "$gitExists" != "true" ]; then
	echo "Couldn't find git in your path. Exiting."
	exit
fi
op=$1
if [ "$op" = "initialize" ]; then
	if [ $# -eq 4 ]; then
		initializeMirror "$2"$3"$4"
	else
		echo "Insufficient number of arguments".
		exit
	fi
elif [ "$op" = "update" ]; then
	if [ $# -eq 2 ]; then
		updateMirror "$2"
	else
		echo "Insufficient number of arguments"
	fi
else
	echo "Insert valid arguments."
	exit
fi





