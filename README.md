# mirrorigno

A tool for automatically mirroring git repositories.

## Requirements

- Powershell if you wish to use the powershell script.
- Bash if you wish to use the bash script.
- Git binary in the system path.

## Usage

Please consult the READMEs inside the bash/ or the ps/ directories depending on what you wish to use.

## License

See LICENSE.txt file in the root directory.
